#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`/..
source variables.sh

ROOT=`pwd`
GIT_ROOT=$ROOT/git
CONF_ROOT=$ROOT/conf
AUTHORS_FILE="$ROOT/1_authors/authors.txt"

function colorprint {
  echo -e "\e[$1m$2\e[0m"
}

function green {
  colorprint 32 "$1"
}
function yellow {
  colorprint 33 "$1"
}
function red {
  colorprint 31 "$1"
}

# cleanup a previous run, this script is a one-off migration and cannot be run multiple times
#red "rm -rf $GIT_ROOT"
#rm -rf $GIT_ROOT
mkdir -p $GIT_ROOT

declare -A git_projects
declare -A git_project_branches
declare -A git_project_branches_rev
for projectname in `ls $CONF_ROOT`; do
  # echo "found $projectname"
  GIT_PROJECT_ROOT="$GIT_ROOT/$projectname"
  branchnames=""
  for branchentry in `cat $CONF_ROOT/$projectname/branches.txt`; do
    git_branch=`echo $branchentry | cut -d '=' -f 1`
    svn_path=`echo $branchentry | cut -d '=' -f 2`
    # echo "found branch entry $git_branch -> $svn_path"
    git_project_branches["$projectname$git_branch"]="$svn_path"
    if [ ! -z "$branchnames" ]; then
      branchnames+=$'\n'
    fi
    branchnames+=$git_branch
  done
  git_projects["$projectname"]="$branchnames"
done

rev=1
latest_rev=`svn info $LOCAL_REPO_ENDPOINT | grep "Revision:" | cut -d ' ' -f2`
echo "HEAD=r$latest_rev"
function find_first_relevant_revision {
  local projectname=${1:-}
  local start=${2:-1}

  local r=$latest_rev
  for branchname in `echo "${git_projects[$projectname]}"`; do
    svn_path="${git_project_branches[$projectname$branchname]}"

    ! path_min_rev=`svn log -r $start:HEAD --limit 1 $LOCAL_REPO_ENDPOINT$svn_path 2> /dev/null | grep -Eo "^r[0-9]+" | grep -Eo "[0-9]+"`
    if [ ${PIPESTATUS[0]} -ne 0 ]; then
      yellow "invalid config: project=$projectname branchname=$branchname svnpath=$svn_path (is this a deleted branch?)"
      continue
    fi
    echo "$svn_path has earliest revision #$path_min_rev"

    if [ "$path_min_rev" -lt "$r" ]; then
      r=$path_min_rev
    fi
  done

  rev=$r
}

for projectname in "${!git_projects[@]}"; do
  green "handling $projectname"
  GIT_PROJECT_ROOT="$GIT_ROOT/$projectname"
  if [[ -d $GIT_PROJECT_ROOT ]]; then
    yellow "already handled, skipping"
    continue
  fi

  mkdir $GIT_PROJECT_ROOT
  cd $GIT_PROJECT_ROOT
  svn_trunk_path="${git_project_branches[${projectname}master]}"
  git svn init $LOCAL_REPO_ENDPOINT --trunk="$svn_trunk_path"
  echo ""

  echo "branches:"
  i=0
  for branchname in `echo "${git_projects[$projectname]}"`; do
    if [ "$branchname" == "master" ];then
      continue
    fi
    i=$((i+1))
    svn_path="${git_project_branches[$projectname$branchname]:1}"
    echo "$branchname -> $svn_path"
    # to be put in .git/config:
    # branches = branches/{name1,name2}:refs/remotes/origin/branches/:namespace/*
    # tags = tags/{tag1,tag2}:refs/remotes/origin/tags/:namespace/*
    # the namespace is to avoid branch name collisions during fetching
    body=${svn_path%/*}
    tail=${svn_path##*/}
    if [ "${svn_path%%/*}" == "tags" ];then
      echo -e "\ttags = $body/{$tail}:refs/remotes/origin/tags/$i/*" >> .git/config
      git_project_branches_rev["$projectname/tags/$i/$tail"]="$branchname"
    else
      # put {} around the last item in the svn_path, so git-svn is happy
      echo -e "\tbranches = $body/{$tail}:refs/remotes/origin/branches/$i/*" >> .git/config
      git_project_branches_rev["$projectname/branches/$i/$tail"]="$branchname"
    fi
  done
  echo ""

  git config svn.authorsfile $AUTHORS_FILE
  find_first_relevant_revision $projectname
  git svn fetch --r $rev:HEAD
  green "svn fetch finished for $projectname"

  # make branches local
  # delete peg-revisions (branches that end in @[0-9]+)
  for b in $(git for-each-ref --format='%(refname:short)' refs/remotes); do
    if ! echo "$b" | grep @ > /dev/null; then
      branch_shortname=${b/origin\//}
      new_branch_name="${git_project_branches_rev[$projectname/$branch_shortname]:-$branch_shortname}"
      echo "pulling remote branch $b to $new_branch_name"
      git branch $new_branch_name refs/remotes/$b
    fi
    git branch -D -r $b
  done


  # rewrite commits and purge empty ones
  git filter-branch --msg-filter 'sed -E "s|([^#]*(#([0-9]+))?.*)|\1\\
\3|" | sed -E "s|^([0-9]+)$|http://cms-project-l1osw-trac-backup.web.cern.ch/cms-project-l1osw-trac-backup/tickets/\1.html|g" | sed -E "s|#([0-9]+)|trac-\1|g" | sed -E "s|git-svn-id: .*@([0-9]+).*|SVN Revision: \1|" | tr -s "\n" "\n"' --prune-empty --tag-name-filter cat -- --all
  ! git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d

  # Time for a sanity check. Are all expected branches present?
  missing=0
  for branchname in `echo "${git_projects[$projectname]}"`; do
    ! git branch | grep $branchname > /dev/null
    if [ ${PIPESTATUS[1]} -ne 0 ]; then
      red "$branchname is missing"
      missing=1
    fi
  done
  if [[ $missing -eq 1 ]];then
    exit 1
  fi

  # transform relevant branches into tags
  for t in $(git for-each-ref --format='%(refname:short)' refs/heads/tags/); do git tag ${t/tags\//} $t && git branch -D $t; done
  git branch -D trunk
done
