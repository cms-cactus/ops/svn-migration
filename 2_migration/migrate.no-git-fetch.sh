#!/bin/bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'
cd `dirname $0`
source variables.sh
ROOT=`pwd`
GIT_ROOT=$ROOT/git
SVN_ROOT=$ROOT/localsvn
CONF_ROOT=$ROOT/conf

function colorprint {
  echo -e "\e[$1m$2\e[0m"
}

function green {
  colorprint 32 "$1"
}
function yellow {
  colorprint 33 "$1"
}
function red {
  colorprint 31 "$1"
}

# cleanup a previous run, this script is a one-off migration and cannot be run multiple times
red "rm -rf $GIT_ROOT $SVN_ROOT"
rm -rf $GIT_ROOT $SVN_ROOT
mkdir -p $GIT_ROOT

# checkout a git branch, create it if needed
function checkout_branch {
  if git branch | grep $1 > /dev/null; then
    git checkout $1 &> /dev/null
  else
    git checkout -b $1 &> /dev/null
  fi
}

authors_file="$ROOT/1_authors/authors.txt"
echo "reading authors list at $authors_file"
authors=`cat $authors_file`
echo ""

declare -A git_projects
declare -A git_project_branches
for git_project_name in `ls $CONF_ROOT`; do
  # echo "found $git_project_name"
  branchnames=""
  for branchentry in `cat $CONF_ROOT/$git_project_name/branches.txt`; do
    git_branch=`echo $branchentry | cut -d '=' -f 1`
    svn_path=`echo $branchentry | cut -d '=' -f 2`
    # echo "found branch entry $git_branch -> $svn_path"
    git_project_branches["$git_project_name$git_branch"]="$svn_path"
    if [ ! -z "$branchnames" ]; then
      branchnames+=$'\n'
    fi
    branchnames+=$git_branch
  done
  git_projects["$git_project_name"]="$branchnames"
done

# create git repos
for projectname in "${!git_projects[@]}"; do
  GIT_PROJECT_ROOT="$GIT_ROOT/$projectname"

  yellow "creating git repository '$projectname':"
  mkdir $GIT_PROJECT_ROOT
  cd $GIT_PROJECT_ROOT
  git init

  echo ""
done

rev=1
latest_rev=`svn info $LOCAL_REPO_ENDPOINT | grep "Revision:" | cut -d ' ' -f2`

# find first commit that touches one of the branches in the configs, starting from rev=$1
declare -A svn_first_revisions
function find_first_relevant_revision {
  start=${1:-1}
  verbose=${2:-1}

  local r=$latest_rev
  for projectname in "${!git_projects[@]}"; do
    for branchname in `echo "${git_projects[$projectname]}"`; do
      svn_path="${git_project_branches[$projectname$branchname]}"
      if [ ${svn_first_revisions[$svn_path]+abc} ] && [ ${svn_first_revisions[$svn_path]} -gt $rev ]; then
        # echo "cached: $svn_path = ${svn_first_revisions[$svn_path]}"
        path_min_rev=${svn_first_revisions[$svn_path]}
      else
        # echo manual
        ! path_min_rev=`svn log -r $start:HEAD --limit 1 --stop-on-copy $LOCAL_REPO_ENDPOINT$svn_path 2> /dev/null | grep -Eo "^r[0-9]+" | grep -Eo "[0-9]+"`
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
          # echo "boom: $svn_path"
          yellow "invalid config: project=$projectname branchname=$branchname svnpath=$svn_path"
          continue
        fi
        # echo "manual search: $svn_path = $path_min_rev"
        svn_first_revisions[$svn_path]=$path_min_rev
        [ $verbose -eq 1 ] && >&2 echo "$svn_path has earliest revision #$path_min_rev"
      fi

      if [ "$path_min_rev" -lt "$r" ]; then
        r=$path_min_rev
      fi
    done
  done

  rev=$r
}


echo "searching for earliest relevant SVN revision"
previous_rev=$rev
find_first_relevant_revision
green "starting at revision $rev"
echo ""
echo "initializing svn repository"
svn checkout -r $rev $LOCAL_REPO_ENDPOINT $SVN_ROOT > /dev/null

# go to the svn repository, and loop over each relevant revision
# in each revision, the file changes will be examined and equivalent commits will be made in
# all relevant git repos & branches
cd $SVN_ROOT
svn up -r$rev &> /dev/null
while commit_txt=`svn log -r$rev` &> /dev/null; do
  echo "handling revision $rev"

  commit_metadata=`echo "$commit_txt" | sed -n '2p'`
  commit_author=`echo $commit_metadata | cut -d '|' -f2 | sed -E "s|^[ ]*||" | sed -E "s|[ ]*$||"`
  commit_date=`echo $commit_metadata | cut -d '|' -f3 | sed -E "s|^[ ]*||" | sed -E "s| \(.*$||"`
  commit_msg=`echo "$commit_txt" | sed '1,3d;$d'`
  echo "author: '$commit_author', date: '$commit_date', msg: $commit_msg"

  svn_log_entry=`svn log -vr $rev`
  if echo "$svn_log_entry" | grep -e "   [ADRM]" > /dev/null; then # skip if no files were touched
    file_changes=`echo "$svn_log_entry" | grep -e "   [ADRM]" | sed -E 's|   [ADRM] ([^(]+).*|\1|'`
    # echo "file changes: $file_changes"

    # go over all git repos, and check if we need to make commits
    for projectname in "${!git_projects[@]}"; do
      GIT_PROJECT_ROOT="$GIT_ROOT/$projectname"

      # determine which branches (if any) need a commit
      declare -A branchhits=()
      # we need the trunk path to do branch merges
      svn_trunk_path="${git_project_branches[${projectname}master]}"
      for git_branch in `echo "${git_projects[$projectname]}"`; do
        svn_path="${git_project_branches[$projectname$git_branch]}"
        while read -r changed_path; do
          if echo $changed_path | grep $svn_path > /dev/null; then
            # echo "git branch $git_branch matched"
            branchhits["$git_branch"]=""
          fi
        done <<< "$file_changes"
      done

      

      if [[ ! -z "${!branchhits[*]}" ]]; then # skip if no relevant branches were touched
        cd $SVN_ROOT
        echo "diffing $previous_rev:$rev"
        svn diff -r $previous_rev:$rev > $ROOT/patchfile
        svn patch $ROOT/patchfile > /dev/null
        previous_rev=$rev
        # svn up -r $rev > /dev/null # set svn repo state to this revision

        # for each affected branch, rsync the current state in the svn repo, and commit
        # additionally, try to detect if this should be a merge commit
        for branchname in "${!branchhits[@]}"; do
          svn_path="${git_project_branches[$projectname$branchname]}"
          cd $GIT_PROJECT_ROOT
          echo "handling $projectname:$branchname"
          checkout_branch $branchname

          if [ "$branchname" == "master" ]; then
            # compare with svn branches
            # if there are branches with no differences, this could be a merge commit
            for git_branch in `echo "${git_projects[$projectname]}" | grep -v "master"`; do
              svn_branch_path="${git_project_branches[$projectname$git_branch]}"
              if [ -d $SVN_ROOT$svn_branch_path ] && [ -d $SVN_ROOT$svn_path ] && \
              diff -r $SVN_ROOT$svn_branch_path $SVN_ROOT$svn_path &> /dev/null; then
                if [[ "$git_branch" != tags/* ]]; then
                  yellow "$git_branch may have merged into master"
                  # if there is nothing to merge (aka false positive), it will do nothing
                  ! git merge -s ours --ff --no-commit $git_branch > /dev/null
                fi
              fi
            done
          fi

          # if branch was deleted, delete it? (then why was it specifically asked for)
          # try to merge to master
          if [ ! -f "$SVN_ROOT/$svn_path" ] && [ ! -d "$SVN_ROOT/$svn_path" ]; then
            yellow "this branch has been deleted"
            # read -p "press enter to continue" -n 1 -r
            checkout_branch master
            ! git merge -s ours --ff --no-commit $branchname > /dev/null
            rsync -a --exclude '.git' --delete --ignore-times $SVN_ROOT$svn_trunk_path/ $GIT_PROJECT_ROOT
          else # branch not deleted, update it
            rsync -a --exclude '.git' --delete --ignore-times $SVN_ROOT$svn_path/ $GIT_PROJECT_ROOT
          fi

          git add -A .
          if ! git diff --cached --exit-code > /dev/null; then
            author=`echo "$authors" | grep $commit_author | cut -d '=' -f2 | tail -c +2`
            # GIT_COMMITTER_DATE="$commit_date" git commit --date "$commit_date" --author="$author" -m "$commit_msg"
            git commit --date "$commit_date" --author="$author" -m "$commit_msg" > /dev/null
            green "committed $commit_msg"
          else
            red "commit skipped, nothing to commit: $commit_msg"
          fi
        done
      else
        echo "no work for $projectname"
      fi
    done
  fi

  previous_rev=$rev
  rev=$((rev+1))
  if [ $latest_rev -lt $rev ]; then
    break
  fi
  echo "finding next relevant revision"
  find_first_relevant_revision $rev 1
  cd $SVN_ROOT
  echo ""
done

green "all commits and branches migrated"
echo "converting tags"

for projectname in "${!git_projects[@]}"; do
  GIT_PROJECT_ROOT="$GIT_ROOT/$projectname"
  cd $GIT_PROJECT_ROOT
  for t in $(git for-each-ref --format='%(refname:short)' refs/heads/tags); do git tag ${t/tags\//} $t && git branch -D $t; done
  green "finished with git repo $projectname"
done

green "done"
