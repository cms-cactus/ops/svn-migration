#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`/..
source variables.sh

ROOT=`pwd`
GIT_ROOT=$ROOT/git
CONF_ROOT=$ROOT/conf

status_code=0
function colorprint {
  echo -e "\e[$1m$2\e[0m"
}

function green {
  colorprint 32 "$1"
}
function yellow {
  colorprint 33 "$1"
}
function red {
  colorprint 31 "$1"
  status_code=1
}

declare -a failures
for projectname in `ls $CONF_ROOT`; do
  echo "checking project $projectname"
  GIT_PROJECT_ROOT="$GIT_ROOT/$projectname"
  if [[ ! -d $GIT_PROJECT_ROOT ]]; then
    red "project not migrated"
    failures+=(" - $projectname")
  else
    branchnames=""
    for branchentry in `cat $CONF_ROOT/$projectname/branches.txt`; do
      git_branch=`echo $branchentry | cut -d '=' -f 1`
      svn_path=`echo $branchentry | cut -d '=' -f 2`
      echo " - $projectname: $git_branch ($svn_path)"
      if [[ ! -d "localsvn$svn_path" ]]; then
        ! svn co $LOCAL_REPO_ENDPOINT$svn_path localsvn$svn_path
      else
        cd localsvn$svn_path
        svn up
        cd -
      fi
      if [[ -d "localsvn$svn_path" ]]; then
        green "  - directory exists"
        cd $GIT_PROJECT_ROOT
        git reset --hard > /dev/null
        git clean -f -d > /dev/null
        git checkout $git_branch &> /dev/null
        rsync -a --delete --ignore-times --exclude '.svn' --exclude '.git' $ROOT/localsvn/$svn_path/* .
        # fix variable substitution during 'svn up'
        ! git ls-files -m | xargs sed -Ei 's#\$(HeadURL|Revision|Author|Date|Id): .*\$#$\1$#g' &> /dev/null
        if git diff --exit-code > /dev/null; then
          green "  - svn and git content identical"
        else
          red "  - svn and git content differ"
          ! git diff --exit-code
          failures+=(" - $projectname: $git_branch ($svn_path)")
        fi
        cd $ROOT
      else
        red "directory does not exist ($svn_path)"
        failures+=(" - $projectname: $git_branch ($svn_path)")
      fi
    done
  fi
done
if [[ $status_code -ne 0 ]]; then
  red "failures:"
  for f in "${failures[@]}"; do
    red "$f"
  done
fi
exit $status_code
