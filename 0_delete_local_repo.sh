#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

source variables.sh

rm -rf $DUMPFILE $LOCAL_REPO_FOLDER
