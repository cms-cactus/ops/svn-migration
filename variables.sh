ROOT_PATH=`realpath $0 | sed -E "s|(.*/svn-migration).*|\1|"`

SVN_REPO_REMOTE="https://svn.cern.ch/reps/cactus/"
DUMPFILE="cactus.dump"
LOCAL_REPO_FOLDER="cactusrepo"
LOCAL_REPO_ENDPOINT="file://$ROOT_PATH/$LOCAL_REPO_FOLDER"
