#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

source ../variables.sh
cd "$(dirname "$0")"

SVNWEB_BASE="https://svnweb.cern.ch"
TICKET_URL_BASE="$SVNWEB_BASE/trac/cactus/ticket/"

mkdir -p tickets
function download_ticket {
  if [ ! -f "tickets/$1.html" ]; then
    curl -o "tickets/$1.html" --fail --silent "$TICKET_URL_BASE/$1"
  fi
}
function download_ticket_csv {
  if [ ! -f "tickets/$1.csv" ]; then
    curl -o "tickets/$1.csv" --fail --silent "$TICKET_URL_BASE/$1?format=csv"
  fi
}

echo "downloading ticket #1"
rm -rf tickets/1.html
download_ticket 1

function download_statics {
  echo "downloading static files"
  mkdir -p static
  JS_LINKS=`cat tickets/1.html | grep '\.js' | sed -E "s|</script>[^$]|</script>\n<|g" | sed -E "s|\s*<script .*src=\"([^\"]+)\".*</script>|\1|g"`
  CSS_LINKS=`cat tickets/1.html | grep '\.css' | sed -E "s|<link ([^>]+)>[^$]|<link \1>\n<|g" | sed -E "s|.*href=\"([^\"]+).*|\1|g"`
  ALL_LINKS=`printf "$JS_LINKS\n$CSS_LINKS"`
  for link in ${ALL_LINKS[*]} 
  do
    FILENAME="static/`basename $link`"
    echo "$FILENAME"
    if [ ! -f "$FILENAME" ]; then
      curl -o "$FILENAME" --fail --silent $SVNWEB_BASE$link
    fi
  done
}
download_statics

LAST_TICKET_NUMBER=`cat tickets/1.html | grep 'rel="last"' | sed -E "s|.*ticket/([0-9]+).*|\1|"`
echo "downloading ticket 1-$LAST_TICKET_NUMBER"

for i in $(seq 1 $LAST_TICKET_NUMBER); do
  echo -en "\033[1K\r"
  echo -n "downloading ticket #$i"
  download_ticket $i
  download_ticket_csv $i
done
echo ""

function fix_tickets {
  echo "correctiong urls in ticket pages"
  sed -E -i -- "s|src=\"[^\"]*/([^/]+\.(js\|png\|gif\|jpg))\"|src=\"/static/\1\"|g" tickets/*.html
  sed -E -i -- "s|href=\"[^\"]*/([^/]+\.css)\"|href=\"/static/\1\"|g" tickets/*.html
  sed -E -i -- "s|/trac/cactus/ticket/([0-9]+)|/tickets/\1.html|g" tickets/*.html
  sed -E -i -- "s|\.html\?format=csv|\.csv|g" tickets/*.html
}
fix_tickets




mkdir -p reports
i=1
while true; do
  echo -en "\033[1K\r"
  echo -n "downloading report #$i"
  if [ -f reports/$i.html ]; then
    report=`cat reports/$i.html`
  else
    report=`curl --fail --silent "$SVNWEB_BASE/trac/cactus/report/1?asc=1&page=$i"`
    echo "$report" > reports/$i.html
  fi
  if [ -z `echo "$report" | grep 'rel="next"'` ]; then
    echo ""
    echo "downloading CSV report"
    curl -o reports/report.csv --fail --silent "$SVNWEB_BASE/trac/cactus/report/1?asc=1&amp;format=csv"
    break
  else
    i=$((i+1))
  fi
done

function fix_reports {
  echo "correctiong urls in report pages"
  sed -E -i -- "s|src=\"[^\"]*/([^/]+\.(js\|png\|gif\|jpg))\"|src=\"/static/\1\"|g" reports/*.html
  sed -E -i -- "s|href=\"[^\"]*/([^/]+\.css)\"|href=\"/static/\1\"|g" reports/*.html
  sed -E -i -- "s|/trac/cactus/ticket/([0-9]+)|/tickets/\1.html|g" reports/*.html
  sed -E -i -- "s|/trac/cactus/report/1\?asc=1&amp;page=([0-9]+)|/reports/\1.html|g" reports/*.html
  sed -E -i -- "s|/trac/cactus/report/1\?asc=1&amp;format=csv|/reports/report.csv|g" reports/*.html
}
fix_reports

sed -E -i -- "s|https://svnweb.cern.ch/trac/cactus|/|g" reports/*.html tickets/*.html
cp -f reports/1.html index.html

echo "done"
