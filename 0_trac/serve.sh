#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

cd "$(dirname "$0")"

python3 -m http.server
