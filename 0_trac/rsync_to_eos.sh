#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

cd "$(dirname "$0")"

./add_prefix.sh "/cms-project-l1osw-trac-backup"
rsync -av ./ cactus@lxplus.cern.ch:/eos/user/c/cactus/www/trac
