#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

cd "$(dirname "$0")"

prefix=${1:-}
if [ -z "$prefix" ];then
  echo "no prefix given"
  exit 1
fi

sed -i -E "s|\"(/static\|/reports\|/tickets)|\"$prefix\1|g" index.html reports/* tickets/*
