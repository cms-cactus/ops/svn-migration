<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  
  

  


  <head>
    <title>
      #1261 (AbstractFactory interface update: dropping ParameterSets in favour of stubs)
     – cactus
    </title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--[if IE]><script type="text/javascript">
      if (/^#__msie303:/.test(window.location.hash))
        window.location.replace(window.location.hash.replace(/^#__msie303:/, '#'));
    </script><![endif]-->
        <link rel="search" href="/trac/cactus/search" />
        <link rel="prev" href="/tickets/1260.html" title="Ticket #1260" />
        <link rel="last" href="/tickets/2373.html" title="Ticket #2373" />
        <link rel="help" href="/trac/cactus/wiki/TracGuide" />
        <link rel="alternate" href="/tickets/1261.csv" type="text/csv" class="csv" title="Comma-delimited Text" /><link rel="alternate" href="/tickets/1261?format=tab" type="text/tab-separated-values" class="tab" title="Tab-delimited Text" /><link rel="alternate" href="/tickets/1261?format=rss" type="application/rss+xml" class="rss" title="RSS Feed" />
        <link rel="next" href="/tickets/1262.html" title="Ticket #1262" />
        <link rel="start" href="/trac/cactus/wiki" />
        <link rel="stylesheet" href="/static/trac.css" type="text/css" /><link rel="stylesheet" href="/static/ticket.css" type="text/css" />
        <link rel="first" href="/tickets/1.html" title="Ticket #1" />
        <link rel="shortcut icon" href="http://cactus.web.cern.ch/cactus/images/cactus_ico.gif" type="image/gif" />
        <link rel="icon" href="http://cactus.web.cern.ch/cactus/images/cactus_ico.gif" type="image/gif" />
    <style id="trac-noscript" type="text/css">.trac-noscript { display: none !important }</style>
      <link type="application/opensearchdescription+xml" rel="search" href="/trac/cactus/search/opensearch" title="Search cactus" />
    <script type="text/javascript">
      var auto_preview_timeout=2.0;
      var form_token="419d621b4776591a365a7398";
      var comments_prefs={"comments_only":"false","comments_order":"oldest"};
    </script>
      <script type="text/javascript" charset="utf-8" src="/static/jquery.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/babel.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/trac.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/search.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/folding.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/wikitoolbar.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/resizer.js"></script>
      <script type="text/javascript" charset="utf-8" src="/static/auto_preview.js"></script>
    <script type="text/javascript">
      jQuery("#trac-noscript").remove();
      jQuery(document).ready(function($) {
        $(".trac-autofocus").focus();
        $(".trac-target-new").attr("target", "_blank");
        setTimeout(function() { $(".trac-scroll").scrollToTop() }, 1);
        $(".trac-disable-on-submit").disableOnSubmit();
      });
    </script>
    <script type="text/javascript" src="/static/threaded_comments.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $("div.description").find("h1,h2,h3,h4,h5,h6").addAnchor(_("Link to this section"));
        $(".foldable").enableFolding(false, true);
      /*<![CDATA[*/
        $("#attachments").toggleClass("collapsed");
        $("#trac-up-attachments").click(function () {
          $("#attachments").removeClass("collapsed");
          return true;
        });
        $("#modify").parent().toggleClass("collapsed");
        $(".trac-topnav a").click(function() { $("#modify").parent().removeClass("collapsed"); });
        /* only enable control elements for the currently selected action */
        var actions = $("#action input[name='action']");
        function updateActionFields() {
          actions.each(function () {
            $(this).siblings().find("*[id]").enable($(this).checked());
            $(this).siblings().filter("*[id]").enable($(this).checked());
          });
        }
        actions.click(updateActionFields);
        updateActionFields();
        function setRevertHandler() {
          $("button.trac-revert").click(function() {
            var div = $("div", this);
            var field_name = div[0].id.substr(7);
            var field_value = div.text();
            var input = $("#propertyform *[name=field_" + field_name + "]");
            if (input.length > 0) {
              if (input.filter("input[type=radio]").length > 0) {
                input.val([field_value]);
              } else if (input.filter("input[type=checkbox]").length > 0) {
                input.val(field_value == "1" ? [field_value] : []);
              } else {
                input.val(field_value);
              }
            } else { // Special case for CC checkbox
              input = $("#propertyform input[name=cc_update]").val([]);
            }
            input.change();
            $(this).closest("li").remove();
            return false;
          });
        }
        setRevertHandler();
        var comment_focused = false;
        $("#comment").focus(function() { comment_focused = true; })
                     .blur(function() { comment_focused = false; });
        $("#propertyform").autoSubmit({preview: '1'}, function(data, reply) {
          var items = $(reply);
          // Update ticket box
          $("#ticket").replaceWith(items.filter('#ticket'));
          // Unthread, unrevert and update changelog
          if (!$('#trac-comments-oldest').checked())
            $('#trac-comments-oldest').click().change();
          $("#changelog").replaceWith(items.filter("#changelog"));
          if ($('#trac-comments-only-toggle').attr('checked'))
            $('#trac-comments-only-toggle').click().attr('checked', true);
          // Show warning
          var new_changes = $("#changelog .trac-new");
          $("#trac-edit-warning").toggle(new_changes.length != 0);
          if (new_changes.length != 0)
            $("#changelog").parent().show().removeClass("collapsed");
          // Update view time
          $("#propertyform input[name='view_time']").replaceWith(items.filter("input[name='view_time']"));
          // Update preview
          var preview = $("#ticketchange").html(items.filter('#preview').children());
          var show_preview = preview.children().length != 0;
          $("#ticketchange").toggle(show_preview);
          setRevertHandler();
          // Collapse property form if comment editor has focus
          if (show_preview && comment_focused)
            $("#modify").parent().addClass("collapsed");
          // Execute scripts to load stylesheets
          items.filter("script").appendTo("head");
        }, "#ticketchange .trac-loading");
        $("#trac-comment-editor").autoSubmit({preview_comment: '1'}, function(data, reply) {
          var comment = $("#trac-comment-editor").next("div.comment").html(reply);
          comment.toggle(comment.children().length != 0);
        }, "#changelog .trac-loading");
        /*]]>*/
      });
    </script>
  </head>
  <body>
    <div id="banner">
      <div id="header">
        <a id="logo" href="/"><img src="/static/cactus_trac_logo.png" alt="" /></a>
      </div>
      <form id="search" action="/trac/cactus/search" method="get">
        <div>
          <label for="proj-search">Search:</label>
          <input type="text" id="proj-search" name="q" size="18" value="" />
          <input type="submit" value="Search" />
        </div>
      </form>
      <div id="metanav" class="nav">
    <ul>
      <li class="first"><a href="/trac/cactus/login">Login</a></li><li><a href="/trac/cactus/prefs">Preferences</a></li><li><a href="/trac/cactus/wiki/TracGuide">Help/Guide</a></li><li class="last"><a href="/trac/cactus/about">About Trac</a></li>
    </ul>
  </div>
    </div>
    <div id="mainnav" class="nav">
    <ul>
      <li class="first"><a href="/trac/cactus/wiki">Wiki</a></li><li><a href="/trac/cactus/timeline">Timeline</a></li><li><a href="/trac/cactus/roadmap">Roadmap</a></li><li><a href="/trac/cactus/browser">Browse Source</a></li><li class="active"><a href="/trac/cactus/report">View Tickets</a></li><li><a href="/trac/cactus/search">Search</a></li><li class="last"><a href="/trac/cactus/blog">Blog</a></li>
    </ul>
  </div>
    <div id="main">
      <div id="ctxtnav" class="nav">
        <h2>Context Navigation</h2>
        <ul>
          <li class="first"><span>&larr; <a class="prev" href="/tickets/1260.html" title="Ticket #1260">Previous Ticket</a></span></li><li class="last"><span><a class="next" href="/tickets/1262.html" title="Ticket #1262">Next Ticket</a> &rarr;</span></li>
        </ul>
        <hr />
      </div>
    <div id="content" class="ticket">
        <div id="ticket" class="trac-content ">
  <div class="date">
    <p>Opened <a class="timeline" href="/trac/cactus/timeline?from=2015-07-01T18%3A13%3A27%2B02%3A00&amp;precision=second" title="See timeline at Jul 1, 2015 6:13:27 PM">3 years ago</a></p>
    <p>Closed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A41%3A15%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:41:15 PM">3 years ago</a></p>
  </div>
  <h2>
    <a href="/tickets/1261.html" class="trac-id">#1261</a>
    <span class="trac-status">
      <a href="/trac/cactus/query?status=closed">closed</a>
    </span>
    <span class="trac-type">
      <a href="/trac/cactus/query?status=!closed&amp;type=enhancement">enhancement</a>
    </span>
    <span class="trac-resolution">
      (<a href="/trac/cactus/query?status=closed&amp;resolution=fixed">fixed</a>)
    </span>
  </h2>
  <h1 id="trac-ticket-title" class="searchable">
    <span class="summary">AbstractFactory interface update: dropping ParameterSets in favour of stubs</span>
  </h1>
  <table class="properties">
    <tr>
      <th id="h_reporter">Reported by:</th>
      <td headers="h_reporter" class="searchable"><a href="/trac/cactus/query?status=!closed&amp;reporter=thea">thea</a></td>
      <th id="h_owner">Owned by:</th>
      <td headers="h_owner"><a href="/trac/cactus/query?status=!closed&amp;owner=thea%2Ccghabrou%2Ckbunkow%2Ckreczko%2Ctsw">thea,cghabrou,kbunkow,kreczko,tsw</a></td>
    </tr>
    <tr>
        <th id="h_priority">
          Priority:
        </th>
        <td headers="h_priority">
              <a href="/trac/cactus/query?status=!closed&amp;priority=major">major</a>
        </td>
        <th id="h_milestone">
          Milestone:
        </th>
        <td headers="h_milestone">
              <a class="closed milestone" href="/trac/cactus/milestone/SWATCH%20suite%20v0.1" title="Completed 3 years ago (Jul 17, 2015 4:22:55 PM)">SWATCH suite v0.1</a>
        </td>
    </tr><tr>
        <th id="h_component">
          Component:
        </th>
        <td headers="h_component">
              <a href="/trac/cactus/query?status=!closed&amp;component=swatch">swatch</a>
        </td>
        <th id="h_version" class="missing">
          Version:
        </th>
        <td headers="h_version">
              <a href="/trac/cactus/query?status=!closed&amp;version="></a>
        </td>
    </tr><tr>
        <th id="h_keywords" class="missing">
          Keywords:
        </th>
        <td headers="h_keywords" class="searchable">
        </td>
        <th id="h_cc" class="missing">
          Cc:
        </th>
        <td headers="h_cc" class="searchable">
        </td>
    </tr>
  </table>
  <div class="description">
    <h3 id="comment:description">
      Description
      <a href="/tickets/1261?action=diff&amp;version=17" class="lastmod trac-diff" title="2015-07-06 14:24:09+00:00">
        (last modified by tsw)
      </a>
    </h3>
    <div class="searchable">
      <p>
The parameters used to create objects via the <tt>AbstractFactory</tt> will be contained within a class derived from the new <tt>AbstractStub</tt> class, rather than within a <tt>XParameterSet</tt>. (At the same time, the <tt>Stub</tt> classes &amp; their member data will no longer inherit from xdata objects, or use the <tt>xdata::Bag</tt>-pattern.)<br />
</p>
<p>
This removes the number of places in code where the stub-data is retreived via dynamic casts, and hardcoded values of key strings; many of these runtime checks will esstentially<br />
</p>
<p>
The signature of <tt>AbstractFactory&lt;T&gt;::make/bake</tt> methods will be updated to:<br />
</p>
<pre class="wiki">template&lt;typename P&gt;
P* make( const std::string&amp; aCreatorId, const AbstractStub&amp; aStub );
</pre>
    </div>
  </div>
</div>
          

        <div>
          <div style="position: relative">
            <form id="prefs" method="get" action="/trac/cactus/prefs" style="position: absolute; right: 0">
              <div id="trac-comments-order">
                <input type="radio" id="trac-comments-oldest" name="trac-comments-order" value="oldest" checked="checked" />
                <label for="trac-comments-oldest">Oldest first</label>
                <input type="radio" id="trac-comments-newest" name="trac-comments-order" value="newest" />
                <label for="trac-comments-newest">Newest first</label>
                <span id="trac-threaded-toggle" style="display: none">
                  <input type="radio" id="trac-comments-threaded" name="trac-comments-order" value="threaded" />
                  <label for="trac-comments-threaded">Threaded</label>
                </span>
              </div>
              <div>
                <input id="trac-comments-only-toggle" type="checkbox" />
                <label for="trac-comments-only-toggle">Comments only</label>
              </div>
            </form>
          </div>
          <h3 class="foldable">Change History <span class="trac-count">(18)</span></h3>
          <div id="changelog">
              <div class="change" id="trac-change-1-1435908696000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:1" class="cnum">
    <a href="#comment:1">comment:1</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-03T09%3A31%3A36%2B02%3A00&amp;precision=second" title="See timeline at Jul 3, 2015 9:31:36 AM">3 years ago</a> by thea
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37425" title="First round of changes: ParameterSet removed from Object; AbstractStub ...">[37425]</a>) First round of changes: <a class="missing wiki">ParameterSet?</a> removed from Object; <a class="missing wiki">AbstractStub?</a> added; factory methods updated - refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a><br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-2-1435908707000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:2" class="cnum">
    <a href="#comment:2">comment:2</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-03T09%3A31%3A47%2B02%3A00&amp;precision=second" title="See timeline at Jul 3, 2015 9:31:47 AM">3 years ago</a> by thea
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37426" title="Stubs updated in processor and system- refs #1261
">[37426]</a>) Stubs updated in processor and system- refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a><br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-3-1435908714000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:3" class="cnum">
    <a href="#comment:3">comment:3</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-03T09%3A31%3A54%2B02%3A00&amp;precision=second" title="See timeline at Jul 3, 2015 9:31:54 AM">3 years ago</a> by thea
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37427" title="system and system/tests cleanup completed - refs #1261
">[37427]</a>) system and system/tests cleanup completed - refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a><br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-4-1435910328000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:4" class="cnum">
    <a href="#comment:4">comment:4</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-03T09%3A58%3A48%2B02%3A00&amp;precision=second" title="See timeline at Jul 3, 2015 9:58:48 AM">3 years ago</a> by thea
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37428" title="All compiles - refs #1261
">[37428]</a>) All compiles - refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a><br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-5-1435919272000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:5" class="cnum">
    <a href="#comment:5">comment:5</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-03T12%3A27%3A52%2B02%3A00&amp;precision=second" title="See timeline at Jul 3, 2015 12:27:52 PM">3 years ago</a> by thea
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37433" title="All unittests working- refs #1261
">[37433]</a>) All unittests working- refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a><br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-6-1436178366000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:6" class="cnum">
    <a href="#comment:6">comment:6</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T12%3A26%3A06%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 12:26:06 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37465" title="SWATCH, refs #1261 : Remove default template parameter from abstract ...">[37465]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Remove default template parameter from abstract factory make method - doesn't work in cell packages, since they can't have C++ 0x flag enabled<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-7-1436178414000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:7" class="cnum">
    <a href="#comment:7">comment:7</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T12%3A26%3A54%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 12:26:54 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37466" title="SWATCH, refs #1261 : Adapting SWATCH cell framework to new stub classes
">[37466]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Adapting SWATCH cell framework to new stub classes<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-8-1436179903000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:8" class="cnum">
    <a href="#comment:8">comment:8</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T12%3A51%3A43%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 12:51:43 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37467" title="SWATCH, refs #1261 : Debugging exception from system creation in ...">[37467]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Debugging exception from system creation in example cell<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-9-1436179981000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:9" class="cnum">
    <a href="#comment:9">comment:9</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T12%3A53%3A01%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 12:53:01 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37468" title="SWATCH, refs #1261 : Set processor stub creator field when stub ...">[37468]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Set processor stub creator field when stub created from boost ptree<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-10-1436182123000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:10" class="cnum">
    <a href="#comment:10">comment:10</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T13%3A28%3A43%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 1:28:43 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37469" title="SWATCH, refs #1261 : Add crate stub getter method
">[37469]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Add crate stub getter method<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-11-1436187413000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:11" class="cnum">
    <a href="#comment:11">comment:11</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T14%3A56%3A53%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 2:56:53 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37470" title="SWATCH, refs #1261 : Minor fixes to crate header and cpp whilst I'm there
">[37470]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Minor fixes to crate header and cpp whilst I'm there<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-12-1436191247000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:12" class="cnum">
    <a href="#comment:12">comment:12</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A00%3A47%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:00:47 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37472" title="SWATCH, refs #1261 : Merge trunk changes into dev branch
">[37472]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Merge trunk changes into dev branch<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-13-1436191273000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:13" class="cnum">
    <a href="#comment:13">comment:13</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A01%3A13%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:01:13 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37473" title="SWATCH, refs #1261 : Merge trunk changes into dev branch
">[37473]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Merge trunk changes into dev branch<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-14-1436191955000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:14" class="cnum">
    <a href="#comment:14">comment:14</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A12%3A35%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:12:35 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37474" title="SWATCH, refs #1261 : Merge dev branch changes into trunk
">[37474]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Merge dev branch changes into trunk<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-15-1436191989000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:15" class="cnum">
    <a href="#comment:15">comment:15</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A13%3A09%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:13:09 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37475" title="SWATCH, refs #1261 : Merge dev branch changes into trunk
">[37475]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Merge dev branch changes into trunk<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-16-1436192034000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:16" class="cnum">
    <a href="#comment:16">comment:16</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A13%3A54%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:13:54 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
    <div class="comment searchable">
      <p>
(In <a class="changeset" href="/trac/cactus/changeset/37476" title="SWATCH, refs #1261 : Delete dev branch
">[37476]</a>) SWATCH, refs <a class="closed ticket" href="/tickets/1261.html" title="enhancement: AbstractFactory interface update: dropping ParameterSets in favour of stubs (closed: fixed)">#1261</a> : Delete dev branch<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-17-1436192649000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:17" class="cnum">
    <a href="#comment:17">comment:17</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A24%3A09%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:24:09 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
  <ul class="changes">
    <li class="trac-field-description">
      <strong class="trac-field-description">Description</strong>
        modified (<a href="/tickets/1261?action=diff&amp;version=17">diff</a>)
    </li>
  </ul>
    <div class="comment searchable">
      <p>
The large change<br />
</p>

    </div>

              </div>
              <div class="change" id="trac-change-18-1436193675000000">
                
  <h3 class="change">
    <span class="threading">
      <span id="comment:18" class="cnum">
    <a href="#comment:18">comment:18</a>
  </span>
    </span>
        Changed <a class="timeline" href="/trac/cactus/timeline?from=2015-07-06T16%3A41%3A15%2B02%3A00&amp;precision=second" title="See timeline at Jul 6, 2015 4:41:15 PM">3 years ago</a> by tsw
  </h3>
  <div class="trac-ticket-buttons">
  </div>
  <ul class="changes">
    <li class="trac-field-resolution">
      <strong class="trac-field-resolution">Resolution</strong>
        set to <em>fixed</em>
    </li><li class="trac-field-status">
      <strong class="trac-field-status">Status</strong>
        changed from <em>new</em> to <em>closed</em>
    </li>
  </ul>
    <div class="comment searchable">
      <p>
The large changes that are part of this ticket are now complete (thanks to Alessandro!), and have now been merged back into the trunk.<br />
</p>
<p>
From my quick inspection of the changes, one minor quibble that I have with the new design is that the constructor of each created object is called with an <tt>AbstractStub</tt> as reference, requiring that reference to be dynamically-casted in the following constructors:<br />
</p>
<ul><li><tt>Processor</tt>
</li><li><tt>DaqTTCManager</tt>
</li><li><tt>Crate</tt>
</li><li><tt>System</tt>
</li></ul><p>
If this dynamic cast fails then a terse <tt>std::bad_cast</tt> exception will be thrown; ideally, either an exception containing more details would be thrown, or (even better) the object's constructor take a const reference of the derived stub object (e.g. <tt>ProcessorStub</tt> for processor classes), and the creator would cast the <tt>AbstractStub</tt> <br />
</p>
<p>
However, this would require to specify the stub type along with the creator type in the regsiter-class macro, and would require that the template class <tt>BasicCreator</tt> has an extra template type.<br />
</p>
<p>
And, in any case, the dynamic cast of the stub would only fail if the creators were given a stub of the incorrect type - the creator/abstract factory method calls are all within the swatch libraries, so risk of error in the short term is small.<br />
</p>
<p>
Therefore, if we do want to improve this behaviour by specifying the object type <strong>and</strong> stub type when each class is registered, then that work can be left for a later tag of SWATCH.<br />
</p>

    </div>

              </div>
          </div>
        </div>
      <div id="help"><strong>Note:</strong> See
        <a href="/trac/cactus/wiki/TracTickets">TracTickets</a> for help on using
        tickets.</div>
    </div>
    <div id="altlinks">
      <h3>Download in other formats:</h3>
      <ul>
        <li class="first">
          <a rel="nofollow" href="/tickets/1261.csv" class="csv">Comma-delimited Text</a>
        </li><li>
          <a rel="nofollow" href="/tickets/1261?format=tab" class="tab">Tab-delimited Text</a>
        </li><li class="last">
          <a rel="nofollow" href="/tickets/1261?format=rss" class="rss">RSS Feed</a>
        </li>
      </ul>
    </div>
    </div>
    <div id="footer" lang="en" xml:lang="en"><hr />
      <a id="tracpowered" href="http://trac.edgewall.org/"><img src="/static/trac_logo_mini.png" height="30" width="107" alt="Trac Powered" /></a>
      <p class="left">Powered by <a href="/trac/cactus/about"><strong>Trac 1.0.8</strong></a><br />
        By <a href="http://www.edgewall.org/">Edgewall Software</a>.</p>
      <p class="right"></p>
    </div>
  </body>
</html>