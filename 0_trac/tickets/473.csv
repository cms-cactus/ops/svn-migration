﻿id,summary,reporter,owner,description,type,status,priority,milestone,component,version,resolution,keywords,cc
473,"uhal , TCP : High latency when number of buffers in dispatch is not multiple of nr_buffers_per_send",tsw,tsw arose cghabrou clazarid,"It seems like there was an unintended side-effect in my implementation of handling multiple buffers in each boost asio TCP send/receive call ...

'''The problem'''

The top-left plot in the `cactus_ticket_473` PNG attachment shows the write latency (excl. TCP connect time) as a function of depth in release v2.1.0, with the depths that fill up all buffers objects shown as dot-dash grey vertical lines. 

I.e. when you cross over each of those dot-dash grey lines from left to right, the number of Buffers needed for the write goes up by 1.

Also, N.B: here (as in the last release) the value of `nr_buffers_per_send` TCP template parameter - i.e. the number of buffers objects that are sent / received accumulate before each boost asio send/receive call - is 3.

The plot shows clearly that there's an extra 300-400 microsecond latency when the number of buffers is not a multiple of `nr_buffers_per_send`

----

'''The cause'''

In the case of a write consisting of 5 packets, the worker thread will currently send the first 3 packets, but then when it reaches the end of `write_callback` even though the other two packets have been created, it won't send them since it will only send if `mDispatchQueue.size() >= nr_buffers_per_send`. 

The last two packets only get sent by the write call within the while loop of `Flush`, and that is only reached after the response for the first 3 packets has come back. 

I.e. the sequence of events is:
 * uHAL sends 3 packets to control hub
 * (Few hundred microsecond wait)
 * ControlHub sends back corresponding 3 response packets
 * uHAL sends last 2 packets to controlhub (due to write call in `Flush`)
 * (Few hundred microsecond wait)
 * ControlHub sends back 2 response packets

... but the sequence of events we want is:
 * uHAL sends 3 packets
 * uHAL sends last 2 packets
 * (Few hundred millisecond wait)
 * uHAL receives first 3 response packets
 * uHAL receives last 2 response packets


----

'''Possible solutions'''

This could be solved in two possible ways:
 1. Add an extra member variable `mFlushStarted`, so that worker thread knows when the main thread has entered `Flush`, and can then send packets regardless of how many are in `mDispatchQueue`
   * This solution has already been implemented locally (the changes aren't so much ... ~ 10 new lines, and a few lines changed)
   * This implementation solves the high latency problem outlined above and doesn't affect the bandwidth for very large block writes/reads
 1. Change the `if( ... ){ write() }` lines at the end of `read_callback` & `write_callback` to send whenever `mDispatchQueue` is non-empty 
   * I also tried implementing this solution. It solved the high latency problem outlined above, but unfortunately decreased the maximum bandwidth for very large block writes/reads to ~ 300Mbit/s


@Andy : Thoughts on which solution is preferable?
",defect,closed,minor,IPbus software - v2.2.0 release,uhal,ipbus sw 2.1,fixed,,
