#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`/..

function colorprint {
  echo -e "\e[$1m$2\e[0m"
}

function green {
  colorprint 32 "$1"
}
function yellow {
  colorprint 33 "$1"
}
function red {
  colorprint 31 "$1"
}

ROOT=`pwd`
GIT_ROOT=$ROOT/git
CONF_ROOT=$ROOT/conf

GIT_PUSH_OPTS="-u"
if [[ "${1:-}" == "--force" ]]; then
  GIT_PUSH_OPTS="-uf"
  red "forced push requested!"
fi

for projectname in `ls $CONF_ROOT`; do
 svn_master_path=`cat $CONF_ROOT/$projectname/branches.txt | grep master | cut -d '=' -f2`
 echo "project $projectname ($svn_master_path)"

 git_url=`cat $CONF_ROOT/$projectname/remote.txt`
 echo "-> $git_url"
 echo ""
done

read -p "Does this look ok? [y/N]" -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  exit 1
fi

read -p "username to use to push: " username
if [[ -z "$username" ]]; then
  exit 1
fi
read -sp "password to use to push: " password
if [[ -z "$password" ]]; then
  exit 1
fi
echo ""

for projectname in `ls $CONF_ROOT`; do
  git_url=`cat $CONF_ROOT/$projectname/remote.txt`
  git_url_secret=`echo "$git_url" | sed -E "s|https://(.*)|https://$username:$password@\1|"`
  cd $GIT_ROOT/$projectname
  ! git remote remove origin &> /dev/null
  git remote add origin $git_url_secret
  git push $GIT_PUSH_OPTS origin --all
  git push $GIT_PUSH_OPTS origin --tags
  green "$projectname push done"
done
