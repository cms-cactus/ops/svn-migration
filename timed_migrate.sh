#!/bin/bash
rm -rf before.txt after.txt migrate.log returncode.txt
date > before.txt
./2_migration/migrate.sh 2>&1 | tee migrate.log
echo $? > returncode.txt
date > after.txt
