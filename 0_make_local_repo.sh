#!/bin/bash
set -o errtrace -o nounset -o pipefail -x
IFS=$'\n\t'

source variables.sh

# This fetches the latest dump of the cactus SVN repo into the $DUMPFILE file.
# This file can be used to create a local copy of the SVN repo, usable by any svn client after unpacking.
# The dump will be unpacked to $LOCAL_REPO_FOLDER
#
# Instead of a full repo, just a bare copy can also be fetched using
# svn export https://username@svn.cern.ch/reps/cactus/

# While the commented-out code below is the more official way of getting a local copy of an svn repo,
# this method is way faster (doesn't need to compile a dump *and then* rebuild the repo)
# furthermore, for some reason, svnrdump doesn't take a whole dump (stopped at revision 21708)
rsync -a cactus@lxplus:/afs/cern.ch/project/svn/reps/cactus .
mv cactus $LOCAL_REPO_FOLDER

read -p "Would you like to create a dump file? (y/N) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  svnrdump dump $LOCAL_REPO_ENDPOINT > $DUMPFILE
fi


# latest_revision=`svn info https://svn.cern.ch/reps/cactus/ | grep "Revision:" | cut -d ' ' -f2`

# if [ -f $DUMPFILE ]; then
#   echo "$DUMPFILE exists, skipping dump"
# else
#   svnrdump dump -r 1:$latest_revision $SVN_REPO_REMOTE > $DUMPFILE
# fi

# if [ -d $LOCAL_REPO_FOLDER ]; then
#   echo "local repo $LOCAL_REPO_FOLDER exists, skipping creation"
# else
#   svnadmin create $LOCAL_REPO_FOLDER
#   svnadmin load $LOCAL_REPO_FOLDER < $DUMPFILE
# fi

# you can create a server on port 3690, but not needed, file:// can be used:
# svnserve -dRr $LOCAL_REPO_FOLDER
# svn checkout svn://localhost:3690

echo "svn repo is available at $LOCAL_REPO_ENDPOINT"
