#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`

rsync -a cactus@lxplus:/afs/cern.ch/project/svn/reps/cactus/ cactusrepo
