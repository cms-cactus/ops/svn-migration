#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

# note that you'll need the cactus password in pass.txt

function get_svn_user {
  result=`ldapsearch -LLL -x -D "CN=cactus,OU=Users,OU=Organic Units,DC=cern,DC=ch" -y pass.txt -h cerndc.cern.ch -b "DC=cern,DC=ch" "cn=$1"`
}

function process_ldap_user {
  full_name=`echo "$result" | grep displayName | cut -c 14-`
  if [ -z "$full_name" ]; then
    manager=`echo "$result" | grep manager | sed -E "s|.*CN=([^,]+),.*|\1|g"`
    if [ -z "$manager" ]; then
      >&2 echo "$1 has no name, and no manager"
      echo "$1 = $1 <$1@cern.ch>"
    else
      >&2 echo "$1 -> $manager (manager)"
      handle_svn_user $manager   
    fi
    return
  fi
  email=`echo "$result" | grep @cern.ch | tail -n1 | rev | cut -d ':' -f1 | rev`
  entry="$full_name <$email>"
  echo "$svn_username = $entry"
}

function handle_svn_user {
  get_svn_user $1
  if [ `echo "$result" | wc -l` -eq 1 ]; then
    >&2 echo "no result for $svn_username"
    echo "$svn_username = $svn_username <$svn_username@cern.ch>"
#    echo "$svn_username = "
  else
    process_ldap_user $1
  fi
}

result=""
svn_username=""
input_file=${1:-authors_ldap.txt}
while read line; do
  svn_username=`echo $line | cut -d ' ' -f 1`
  if [[ $line =~ ^.+[[:space:]]=[[:space:]].+[[:space:]]\<.+@.+\>$ ]];then
    echo "$line"
  else
    handle_svn_user $svn_username
  fi
done < $input_file
