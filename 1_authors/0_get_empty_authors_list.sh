#!/bin/bash
set -o errtrace -o nounset -o pipefail
IFS=$'\n\t'

source ../variables.sh

svn log --xml --quiet $LOCAL_REPO_ENDPOINT | grep author | sort -u | perl -pe 's/.*>(.*?)<.*/$1 = /' > authors_empty.txt
